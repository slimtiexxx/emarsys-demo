const monthNames = ["January", "February", "March", "April", "May", "Juni", "July", "August", "September", "Oktober", "November", "December"];

const TODAY = new Date('September 22, 2017 16:30');

const CURRENT_TIME = (TODAY.getHours() * 60) + TODAY.getMinutes();
const START_WORK_TIME = 9 * 60;
const END_WORK_TIME = 17 * 60;

const TOTAL_WORK_TIME = (END_WORK_TIME - START_WORK_TIME);


function newTicket(estimatedHour, estimatedMinute) {
    let finishDate = new Date('September 22, 2017 16:30');

    if (isWorkDay(TODAY.getDay())) {

        if (isWorkTime(CURRENT_TIME)) {

            let estimatedTime = (estimatedHour * 60) + estimatedMinute;
            let remainingTime = (END_WORK_TIME - CURRENT_TIME);


            if (remainingTime > estimatedTime) {
                finishDate.setMinutes(finishDate.getMinutes() + estimatedTime);

                console.log('The issue will finish today, at ' + finishDate.getHours() + ':' + finishDate.getMinutes())

            } else {

                let difference = estimatedTime - remainingTime;
                let days = difference / TOTAL_WORK_TIME;


                finishDate.setMinutes((finishDate.getMinutes() + remainingTime)); // End of the first day
                finishDate.setMinutes(((finishDate.getMinutes() - TOTAL_WORK_TIME))); // Set to work start and add the difference

                finishDate.setDate(finishDate.getDate() + 1);

                if (Math.floor(days) == 0) {

                    skipWeekends(finishDate);

                    finishDate.setMinutes(finishDate.getMinutes() + difference);

                    skipWeekends(finishDate);

                } else {

                    skipWeekends(finishDate);

                    let remainingDifference = days % Math.floor(days) * TOTAL_WORK_TIME;

                    finishDate.setDate(finishDate.getDate() + Math.floor(days));
                    finishDate.setMinutes(finishDate.getMinutes() + remainingDifference);

                    skipWeekends(finishDate);
                }


                if (((finishDate.getHours() * 60) + finishDate.getMinutes()) > END_WORK_TIME) {
                    finishDate.setHours(finishDate.getHours() + (24 - (TOTAL_WORK_TIME * 60)));
                }

                if (((finishDate.getHours() * 60) + finishDate.getMinutes()) < START_WORK_TIME) {
                    finishDate.setHours(finishDate.getHours() + (24 - (TOTAL_WORK_TIME * 60)));
                }


                console.log('The issue will finish on ' + monthNames[finishDate.getMonth()] + ' ' + finishDate.getDate() + '. at ' + finishDate.getHours() + ':' + finishDate.getMinutes())
            }
        }
    }
}

let form = document.getElementById('submitform');

form.addEventListener("submit", function(e) {
    let hourInput = document.getElementById('formHours');
    let minuteInput = document.getElementById('formMinutes');
    let hours = Number(hourInput.value);
    let minutes = Number(minuteInput.value);

    newTicket(hours, minutes);
    e.preventDefault();
});

function isWorkDay(day) {
    return (day != 6 && day != 7 && day != 0)
}

function skipWeekends(date) {

    // Satudray
    if (date.getDay() == 6) {
        date.setDate(date.getDate() + 2);
    }

    // Sunday
    if (date.getDay() == 0) {
        date.setDate(date.getDate() + 1);
    }
}

function isWorkTime(time) {
    return (time <= END_WORK_TIME && time >= START_WORK_TIME)
}
